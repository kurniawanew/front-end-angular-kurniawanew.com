import { Component, OnInit, OnDestroy } from '@angular/core';
declare var $: any;
declare var Pace: any;
declare var toastr: any;

import { AuthService } from '..//auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

	bodyClasses = 'hold-transition login-page';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];

  constructor(
  	private authService: AuthService,
  	private router: Router
  ) { }

  ngOnInit() {
    // add the the body classes
    this.body.classList.add('hold-transition');
    this.body.classList.add('login-page');
    $(document).ready(function () {
    	$("#username").focus();
    });
  }

   ngOnDestroy() {
    // remove the the body classes
    this.body.classList.remove('hold-transition');
    this.body.classList.remove('login-page');
  }

  LogIn() {
  	var userName = $("#username").val();
  	var pass = $("#password").val();
  	var param = {
  		username: userName,
			password: pass
  	};
	  var redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/admin/dashboard';
	  var this_ = this;
  	Pace.track(function(){
	  	this_.authService.login(JSON.stringify(param)).subscribe(resp => {
	  		localStorage.setItem("token", resp.body.token);
	  		localStorage.setItem("name", resp.body.name);
	  		localStorage.setItem("email", resp.body.email);
	    	this_.router.navigate([redirect]);
	    	toastr.clear();
	  	});
		});
  }

  keyDownFunction(event) {
  if(event.keyCode == 13) {
    this.LogIn();
  }
}

}
