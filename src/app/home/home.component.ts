import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  	$("#myCarousel").carousel();
    $(".navbar").css("background-color", "transparent");
	$("title").text("Home - Blognya Kurniawan");
  }

}
