import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';

declare var toastr: any;

import { 
  HttpClient, 
  HttpHeaders, 
  HttpErrorResponse
} from '@angular/common/http'; 

@Injectable()

export class AuthService {
  isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    toastr.options.timeOut = 0;
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      // console.error('An error occurred:', error.error.message);
      toastr.error(error.error.message, 'Error');
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      // console.error(
      //   `Backend returned code ${error.status}, ` +
      //   `body was: ${error.error.message}`);
      toastr.error(error.error.message, 'Error');
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  }

  login(param: any) {
    return this.http.post(environment.api + 'public/login', param, {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        // 'Authorization': 'my-auth-token'
      }),
      observe: 'response',
      responseType: 'json'
    })
    .pipe(
      catchError(this.handleError)
    );
  }

  logout(): void {
    this.isLoggedIn = false;
  }

  checkToken() {
    var token = localStorage.getItem("token");
  }
}