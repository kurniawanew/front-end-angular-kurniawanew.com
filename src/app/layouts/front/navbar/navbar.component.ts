import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent { 
	Menus: any;

    constructor() {}

    ngOnInit() {
    	this.Menus = [
    		{ name: "HOME", path: "/home" },
    		{ name: "BLOG", path: "/blog" }
    	];
	  	$("body").on("click", "#tombolSidebar, .linkSide",function(){
			$("#mySidenav").toggleClass("SideHide").toggleClass("SideShow");
	  	});
	}
}