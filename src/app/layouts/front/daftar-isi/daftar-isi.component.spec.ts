import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarIsiComponent } from './daftar-isi.component';

describe('DaftarIsiComponent', () => {
  let component: DaftarIsiComponent;
  let fixture: ComponentFixture<DaftarIsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarIsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarIsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
