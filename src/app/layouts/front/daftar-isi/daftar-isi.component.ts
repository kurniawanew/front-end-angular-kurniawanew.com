import { Component, OnInit, AfterContentChecked } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-daftar-isi',
  templateUrl: './daftar-isi.component.html',
  styleUrls: ['./daftar-isi.component.css']
})
export class DaftarIsiComponent implements AfterContentChecked {

	ngOnInit() {
		$('body').scrollspy({
        target: "#daftarIsiLinks",
        offset: "50"
    });

		$("body").on('click', '.linkJump', function(event) {
      event.preventDefault();
      var hash = $(this).data("hash");
      $('html, body').animate({
          scrollTop: $("#"+hash).offset().top
      }, 1000);
    });
	}

  ngAfterContentChecked() {
  	let ini = this;
  	$("#daftarIsiLinks").empty();
  	$(".daftarIsi").each(function(i, el){
      var this_text = $(el).text();
      var hash_target = this_text.toString().toLowerCase() //slugify
				.replace(/\s+/g, '-')           // Replace spaces with -
				.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
				.replace(/\-\-+/g, '-')         // Replace multiple - with single -
				.replace(/^-+/, '')             // Trim - from start of text
				.replace(/-+$/, '');
      var url_path = window.location.pathname;

      $(el).attr("id", hash_target);

      $("#daftarIsiLinks").append(
        $("<li>", {"class": "list-group-item"}).html(
          $("<a>", {
            "href": "#"+hash_target, 
            "data-hash": hash_target, 
            "class":"linkJump",
            "style": "color: "+localStorage.getItem("catColor")
          }).html(this_text)
        )
      );
    });
  }
}
