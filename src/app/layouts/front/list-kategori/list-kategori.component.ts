import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../blog.service';

@Component({
  selector: 'app-list-kategori',
  templateUrl: './list-kategori.component.html',
  styleUrls: ['./list-kategori.component.css']
})
export class ListKategoriComponent implements OnInit {

  CategoryLists: any;
  CategoryIndex: number = 1;

  constructor(
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.blogService.getListKategoriAndArticle().subscribe(data => {
      this.CategoryLists = data.body;
    });

  }

}
