import { AppRoutingModule } from '..//app-routing.module';
import { AdminRoutingModule } from '..//admin/admin-routing.module';
import { BlogRoutingModule } from '..//blog/blog-routing.module';
import { RecaptchaModule } from 'ng-recaptcha'

import { ListKategoriComponent } from './front/list-kategori/list-kategori.component';
import { DaftarIsiComponent } from './front/daftar-isi/daftar-isi.component';
import { NavbarComponent } from './front/navbar/navbar.component';
import { FooterComponent } from './front/footer/footer.component';
import { SidenavComponent } from './front/sidenav/sidenav.component';
import { CommentComponent } from './front/comment/comment.component';

import { AdminControlSidebarComponent } from './admin/admin-control-sidebar/admin-control-sidebar.component';
import { AdminFooterComponent } from './admin/admin-footer/admin-footer.component';
import { AdminLeftSideComponent } from './admin/admin-left-side/admin-left-side.component';
import { AdminHeaderComponent } from './admin/admin-header/admin-header.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogService } from '../blog.service';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    AdminRoutingModule,
    BlogRoutingModule,
    RecaptchaModule.forRoot()
  ],
  declarations: [
    ListKategoriComponent,
    DaftarIsiComponent,
    NavbarComponent,
    FooterComponent,
    SidenavComponent,
    CommentComponent,
    AdminControlSidebarComponent,
    AdminFooterComponent,
    AdminLeftSideComponent,
    AdminHeaderComponent
  ],
  exports: [
    ListKategoriComponent,
    DaftarIsiComponent,
    NavbarComponent,
    FooterComponent,
    SidenavComponent,
    CommentComponent,
    AdminControlSidebarComponent,
    AdminFooterComponent,
    AdminLeftSideComponent,
    AdminHeaderComponent
  ],
  providers: [BlogService]
})
export class LayoutsModule { }
