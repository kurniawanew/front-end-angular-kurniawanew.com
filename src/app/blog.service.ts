import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()

export class BlogService {

  constructor(private http: HttpClient) { }

  getBlogList() {
    return this.http.get(environment.api + 'public/article/page/1', {observe: "response", responseType: 'json'});
  }

  getListKategoriAndArticle(){
    return this.http.get(environment.api + 'public/category/list-article', {observe: "response", responseType: 'json'});
  }

  getArtikel(slug: string): any{
    return this.http.get(environment.api + 'public/article/' + slug, {observe: "response", responseType: 'json'});
  }

}
