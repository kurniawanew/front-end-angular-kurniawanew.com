import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {

  bodyClasses = 'skin-red-light sidebar-mini fixed';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];
  isNotLogin: boolean;

  constructor(private route: Router) { }

  ngOnInit() {
    // add the the body classes
    this.body.classList.add('skin-red-light');
    this.body.classList.add('sidebar-mini');
    this.body.classList.add('fixed');
    this.isNotLogin = this.route.url != "/admin/login" ? true : false;
  }

   ngOnDestroy() {
    // remove the the body classes
    this.body.classList.remove('skin-red-light');
    this.body.classList.remove('sidebar-mini');
    this.body.classList.remove('fixed');
  }

}
