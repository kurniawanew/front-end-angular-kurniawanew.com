import { AdminRoutingModule } from './admin-routing.module';

import { AdminContentComponent } from './admin-content/admin-content.component';
import { AdminDashboard1Component } from './admin-dashboard1/admin-dashboard1.component';
import { AdminDashboard2Component } from './admin-dashboard2/admin-dashboard2.component';
import { AdminComponent } from './admin.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutsModule } from "../layouts/layouts.module";

import { AuthGuard } from '..//auth-guard.service';
import { AuthService }      from '..//auth.service';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    LayoutsModule
  ],
  declarations: [
    AdminComponent,
    AdminContentComponent,
    AdminDashboard1Component,
    AdminDashboard2Component
  ],
  exports: [AdminComponent],
  providers: [
    AuthGuard,
    AuthService
  ]
})
export class AdminModule { }
