import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { AdminDashboard1Component } from './admin-dashboard1/admin-dashboard1.component';
import { AdminDashboard2Component } from './admin-dashboard2/admin-dashboard2.component';

import { AuthGuard } from '..//auth-guard.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
        	{
        		path: "",
        		canActivateChild: [AuthGuard],
		        children: [
		          {
		            path: '',
		            redirectTo: 'dashboard',
		            pathMatch: 'full'
		          },
		          {
		            path: 'dashboard',
		            component: AdminDashboard1Component
		          }
		        ]
        	}
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
