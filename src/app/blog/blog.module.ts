import { BlogRoutingModule } from './blog-routing.module';

import { BlogListComponent } from './blog-list/blog-list.component';
import { ArtikelComponent } from './artikel/artikel.component';
import { BlogComponent } from './blog.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutsModule } from "../layouts/layouts.module";

import { BlogService } from '../blog.service';

@NgModule({
  imports: [
    BlogRoutingModule,
    CommonModule,
    LayoutsModule
  ],
  declarations: [
    BlogComponent,
    BlogListComponent,
    ArtikelComponent
  ],
  providers: [BlogService]
})
export class BlogModule { }
