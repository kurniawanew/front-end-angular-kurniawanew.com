import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../blog.service';
declare var $: any;

@Component({
  selector: 'app-blog',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {

  BlogLists: any;

  constructor(private blogService: BlogService) {
    
  }

  ngOnInit(): void {
    this.blogService.getBlogList().subscribe(resp => {
      this.BlogLists = resp.body;
    });
    // this.BlogLists = this.blogService.getBlogList();
    $(".navbar").css("background-color", "#cc0000");
    $("title").text("Daftar Artikel - Blognya Kurniawan");
  }
}
