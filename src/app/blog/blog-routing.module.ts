import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BlogComponent } from './blog.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { ArtikelComponent } from './artikel/artikel.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'blog',
        component: BlogComponent,
        children: [
          { path: '', component: BlogListComponent },
          { path: ':slug', component: ArtikelComponent }
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class BlogRoutingModule { }