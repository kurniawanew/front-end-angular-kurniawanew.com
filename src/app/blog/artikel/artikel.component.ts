import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../../blog.service';
declare var $: any;
declare var hljs: any;

@Component({
  selector: 'app-artikel',
  templateUrl: './artikel.component.html',
  styleUrls: ['./artikel.component.css']
})

export class ArtikelComponent implements OnInit {

  ArticleDetail: any[] = [];
  CurrentStyles: {};

  constructor(
    private route: ActivatedRoute,
    private blogService: BlogService
  ) { }

  ngOnInit(){
    this.route.params.subscribe(
        params => {
          let slug: string = this.route.snapshot.paramMap.get('slug');
          
          this.blogService.getArtikel(slug).subscribe(resp => {
            this.ArticleDetail = resp.body;
            this.CurrentStyles = {'background-color': resp.body.category.color};
            localStorage.setItem("catColor", resp.body.category.color);
            $(document).ready(function () {
              $(".navbar").css("background-color", resp.body.category.color);
              $("#accordion").find(".collapse").each(function(i, el){
              $("a").css("color", resp.body.category.color);
                if($(el).attr("id") == "Collapse"+resp.body.category.id) {
                  $(el).addClass("in");
                } else {
                  $(el).removeClass("in");
                }
              });
              $("title").text(resp.body.title + " - Blognya Kurniawan");
            });
          });
        }
    );
  }

  ngAfterContentChecked() {
    hljs.initHighlightingOnLoad();
    $(".daftarIsi").css("padding-top", "50px");
  }
}
